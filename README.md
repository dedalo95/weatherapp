# Weather and Business App
This simple back-end in Node.js provides weather and business information about the cities that are passed in input.

## Setup
Run ```npm install``` command to install dependencies. 
After installing the dependencies, run  ```node app.js ``` command to launch the application.
The server will listen on port 3000

## Testing the app
For testing the application you have to make a Post request to the endpoint  ```/info ``` giving an array of cities as parameter.
The request in POSTMAN will be like that: 
```
http://localhost:3000/info?cities=["Turin","Tokyo","Berlin","New York","Dublin"]
```
You can send from zero to five cities in the array. 
The output will be a JSON like this one:
```
[
    {
        "Alessandria":{
            "weather":[
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "businesses":[
                {
                    "id": "wXrhkErjwqPIeEri6vCu9w",
                    "alias": "locanda-dellolmo-bosco-marengo",
                    "name": "Locanda Dell'Olmo",
                    "image_url": "",
                    "is_closed": false,
                    "url": "https://www.yelp.com/biz/locanda-dellolmo-bosco-marengo?adjust_creative=b9LsSfkkPkRD2fXYBGuaUg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=b9LsSfkkPkRD2fXYBGuaUg",
                    "review_count": 1,
                    "categories": [
                        {
                            "alias": "restaurants",
                            "title": "Restaurants"
                        }
                    ],
                    "rating": 4,
                    "coordinates": {
                        "latitude": 44.8217,
                        "longitude": 8.67756
                    },
                    "transactions": [],
                    "location": {
                        "address1": "Piazza Mercato 6",
                        "address2": "",
                        "address3": "",
                        "city": "Bosco Marengo",
                        "zip_code": "15062",
                        "country": "IT",
                        "state": "AL",
                        "display_address": [
                            "Piazza Mercato 6",
                            "15062 Bosco Marengo",
                            "Italy"
                        ]
                    },
                    "phone": "+390131299186",
                    "display_phone": "+39 0131 299186",
                    "distance": 1855.2071635637922
                }
            ]
        },
        "City2":{
            "weather":[],
            "businesses":[]
        },
        [...]
    }
]
```
