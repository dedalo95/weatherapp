var express= require("express");
const { promises } = require("fs");
var app = express();
const dotenv = require('dotenv').config();
const request = require('request');
const { deserialize } = require("v8");
//endpoint info
app.post("/info",function(req,res){
    console.log(req.query.cities);
    cities = JSON.parse(req.query.cities);
    //if there are more than 5 cities in the array
    if(cities.length>5){
        res.send("You can send up to 5 cities. Try again");
    }
    else{
        var promises = [];
        var arrayResult = [];
        cities.forEach(city => {
          var dictionary = {};
          //make a promise
          promises.push(new Promise((resolve,reject)=>{
            //request to weather API
            request({
                url: 'http://api.openweathermap.org/data/2.5/weather?q='+city+"&appid="+process.env.WEATHER_API,
                rejectUnauthorized: false
              }, (err, response)=> {
                    if(err) {
                      reject();
                    } 
                    else {
                      try{
                        //store latitude and longitude in two variables
                        latitude = JSON.parse(response.body).coord.lat;
                        longitude = JSON.parse(response.body).coord.lon;
                        //send request to YELP
                        request({
                          url: 'https://api.yelp.com/v3/businesses/search?latitude='+latitude+'&longitude='+longitude,
                          headers: {
                            'Authorization': 'Bearer '+process.env.YELP_API
                          },
                          rejectUnauthorized: false
                        }, (err, resYelp) => {
                              if(err) {
                                reject();
                              } else {
                                //Push to variable "dictionary" weather and business data
                                dictionary[city] = {"weather":JSON.parse(response.body).weather,"business":JSON.parse(resYelp.body).businesses}
                                //push dictionary to array result
                                arrayResult.push(dictionary);
                                resolve();
                              }
                        
                        });
                      }
                      catch(e){
                        //if data format is incorrect
                        dictionary[city] = "No data";
                        //push to array entry with "no data value"
                        arrayResult.push(dictionary);
                        resolve();
                      }
                    }
              });
        }));
      });
      //at the end
      Promise.all(promises).then(values => {
        //send array of results
        res.send(arrayResult);
      },reason =>{
        res.send("Something went wrong! Retry later");
      });
    }
});

app.listen(3000,function () {
    var host = "localhost";
    console.log("In ascolto a http://%s:%s", host,3000);
 });
 
